package com.boss.bossframeworkweb.config.utils;

public enum BodyType {
	// xml请求包体
	Type_XML,
	// json请求包体
	Type_JSON
}
