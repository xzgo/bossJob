package com.boss.bossframeworkweb.config.constants;

import lombok.Getter;

@Getter
public enum ResultCodeEnum {

    ILLEGAL_CALLBACK_REQUEST_ERROR(false,21013,"回调地址有问题"),
    FETCH_ACCESSTOKEN_FAILD(false,21014,"获取ACCESSTOKEN失败"),
    FETCH_USERINFO_ERROR(false,21015,"获取用户失败"),
    URL_ENCODE_ERR(false,21009,"地址错误"),

    FETCH_PLAYAUTH_ERROR(false,21010,"视频点播错误"),
    REFRESH_VIDEO_PLAYAUTH_ERROR(false,21012,"刷新视频播放错误"),
    FETCH_VIDEO_PLAYAUTH_ERROR(false, 21011,"获取播放凭证失败"),


    VIDEO_UPLOAD_ALIYUN_ERROR(false, 21009,"视频上传云服务器失败"),
    VIDEO_UPLOAD_TOMCAT_ERROR(false, 21010,"视频上传缓存服务器失败"),
    VIDEO_DELETE_ALIYUN_ERROR(false, 21011,"视频删除失败"),
    USERLOGIN_INVALID_ERROR(false,21007,"登录信息过期，请重新登录"),
    USERUNLOGIN_ERROR(false,21008,"用户未登录，请重新登录"),
    SUCCESS(true,20000,"成功"),
    UNKNOWN_REASON(false, 20001, "未知错误"),
    BAD_SQL_GRAMMAR(false, 21001, "sql语法错误"),
    JSON_PARSE_ERROR(false, 21002, "json解析异常"),
    PARAM_ERROR(false, 21003, "参数不正确"),
    FILE_UPLOAD_ERROR(false, 21004, "文件上传错误"),
    EXCEL_DATA_IMPORT_ERROR(false, 21005, "Excel数据导入错误");
    private Boolean success;
    private Integer code;
    private String message;

    private ResultCodeEnum(Boolean success, Integer code, String message)
    {
        this.success = success;
        this.code = code;
        this.message = message;
    }

}
