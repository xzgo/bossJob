package com.boss.bossframeworkweb.service.impl;

import com.boss.bossframeworkweb.mapper.PositionGzMapper;
import com.boss.bossframeworkweb.pojo.PositionGz;
import com.boss.bossframeworkweb.pojo.positions;
import com.boss.bossframeworkweb.pojo.workplace;
import com.boss.bossframeworkweb.service.PositionGzService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class PositionGsServiceimpl  implements PositionGzService {
    @Autowired
    private PositionGzMapper positionGzMapper;

    @Override
    public List<PositionGz> positionGzlist() {
        return positionGzMapper.positionGzlist();
    }

    @Override
    public List<PositionGz> positionGzlist2() {
        return positionGzMapper.positionGzlist2();
    }

    @Override
    public List<workplace> gonglist() {
        return positionGzMapper.gonglist();
    }

}
