package com.boss.bossframeworkweb.service;

import com.boss.bossframeworkweb.pojo.ImageAds;

import java.util.List;

public interface ImageAdsService {
    /**
     * 查询主页图片
     * @return
     */
    List<ImageAds>FindImage();
}
