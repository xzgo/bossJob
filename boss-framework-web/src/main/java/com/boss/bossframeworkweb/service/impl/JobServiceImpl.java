package com.boss.bossframeworkweb.service.impl;

import com.boss.bossframeworkweb.mapper.Job1Mapper;
import com.boss.bossframeworkweb.pojo.job1;
import com.boss.bossframeworkweb.pojo.job2;
import com.boss.bossframeworkweb.pojo.job3;
import com.boss.bossframeworkweb.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class JobServiceImpl implements JobService {
    @Autowired
    private  Job1Mapper job1Mapper;

    @Override
    public List<job1> list() {
        return job1Mapper.list();
    }

}
