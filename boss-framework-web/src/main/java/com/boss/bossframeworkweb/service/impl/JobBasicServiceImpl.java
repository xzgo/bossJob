package com.boss.bossframeworkweb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boss.bossframeworkweb.mapper.JobBasicMapper;
import com.boss.bossframeworkweb.pojo.JobBasic;
import com.boss.bossframeworkweb.pojo.WorkTitle;
import com.boss.bossframeworkweb.service.JobBasicService;
import org.springframework.stereotype.Service;

@Service
public class JobBasicServiceImpl extends ServiceImpl<JobBasicMapper,JobBasic> implements JobBasicService {


    @Override
    public int addJobBasic(JobBasic jobBasic) {
        return baseMapper.insert(jobBasic);
    }

}
