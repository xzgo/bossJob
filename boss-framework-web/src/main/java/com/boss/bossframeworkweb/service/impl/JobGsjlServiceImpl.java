package com.boss.bossframeworkweb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boss.bossframeworkweb.mapper.JobGsjlMapper;
import com.boss.bossframeworkweb.pojo.JobGsjl;
import com.boss.bossframeworkweb.service.JobGsjlService;
import org.springframework.stereotype.Service;

@Service
public class JobGsjlServiceImpl  extends ServiceImpl<JobGsjlMapper, JobGsjl> implements JobGsjlService {

    /**
     * 发布职位
     * @param gsjl
     * @return
     */

    @Override
    public int AddJobGsjl(JobGsjl gsjl) {
        return baseMapper.insert(gsjl);
    }
}
