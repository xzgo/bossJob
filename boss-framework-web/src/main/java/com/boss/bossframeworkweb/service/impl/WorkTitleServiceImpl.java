package com.boss.bossframeworkweb.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boss.bossframeworkweb.mapper.WorkTitleMapper;
import com.boss.bossframeworkweb.pojo.WorkTitle;
import com.boss.bossframeworkweb.service.WorkTitleService;
import org.springframework.stereotype.Service;

@Service
public class WorkTitleServiceImpl extends ServiceImpl<WorkTitleMapper,WorkTitle> implements WorkTitleService {


    @Override
    public int addWorkTitle(WorkTitle workTitle) {
        return baseMapper.insert(workTitle);
    }
}
