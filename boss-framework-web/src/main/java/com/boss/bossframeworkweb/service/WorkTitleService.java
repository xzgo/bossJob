package com.boss.bossframeworkweb.service;

import com.boss.bossframeworkweb.pojo.WorkTitle;

public interface WorkTitleService {

    //新增期望工作
    int addWorkTitle(WorkTitle workTitle);
}
