package com.boss.bossframeworkweb.service.impl;

import com.boss.bossframeworkweb.mapper.ImageAdsMapper;
import com.boss.bossframeworkweb.pojo.ImageAds;
import com.boss.bossframeworkweb.service.ImageAdsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ImageAdsServiceImpl implements ImageAdsService {

    @Autowired
    private ImageAdsMapper imageAds;

    @Override
    public List<ImageAds> FindImage() {
        return imageAds.FindImage();
    }
}
