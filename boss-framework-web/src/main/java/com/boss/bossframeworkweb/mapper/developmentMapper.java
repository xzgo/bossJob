package com.boss.bossframeworkweb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boss.bossframeworkweb.pojo.development;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface developmentMapper extends BaseMapper<development> {
    List<development>findAllDevelopment();
}
