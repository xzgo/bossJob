package com.boss.bossframeworkweb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boss.bossframeworkweb.pojo.positions;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface positionsMapper extends BaseMapper<positions> {
    //热门职位
    List<positions> getlist();
}
