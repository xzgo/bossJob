package com.boss.bossframeworkweb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boss.bossframeworkweb.pojo.province;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface provinceMapper extends BaseMapper<province> {
    //查询省份
    List<province>getAll();
}
