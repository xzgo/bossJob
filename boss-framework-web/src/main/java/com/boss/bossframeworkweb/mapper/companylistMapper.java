package com.boss.bossframeworkweb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boss.bossframeworkweb.pojo.Page;
import com.boss.bossframeworkweb.pojo.company;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

@Mapper
public interface companylistMapper extends BaseMapper<company> {

    List<company> companyPageList(Page page);

    int totalCount(@Param("dId") Integer dId, @Param("fId") Integer fId);

    List<company> companyPageListcha();

}
