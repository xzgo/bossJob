package com.boss.bossframeworkweb.mapper;

import com.boss.bossframeworkweb.pojo.ImageAds;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ImageAdsMapper {
    /**
     * 查询主页图片
     * @return
     */
    List<ImageAds> FindImage();
}
