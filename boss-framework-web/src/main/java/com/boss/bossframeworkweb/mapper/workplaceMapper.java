package com.boss.bossframeworkweb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boss.bossframeworkweb.pojo.workplace;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface workplaceMapper extends BaseMapper<workplace> {
    List<workplace>workplacelist();
}
