package com.boss.bossframeworkweb.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boss.bossframeworkweb.pojo.job1;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface Job1Mapper extends BaseMapper<job1> {

    public List<job1>list();
}
