package com.boss.bossframeworkweb.controller.admin;

import com.boss.bossframeworkweb.pojo.JobSearch;
import com.boss.bossframeworkweb.pojo.UserLogin;
import com.boss.bossframeworkweb.service.JobSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 职位订阅
 */
@Controller
public class JobSearchController {

    //职位订阅
    @Autowired
    private JobSearchService jobSearchService;


    /**
     * 职位订阅
     * @param jobSearch
     * @param request
     * @return
     */
    @PostMapping("/addjobSearch")
    public String AddjobSearch(JobSearch jobSearch,HttpServletRequest request) {
        int i = jobSearchService.AddJobSearch(jobSearch);
        if (i>0){
            return "/companylist";
        }
        request.setAttribute("errMsg","新增失败");
        return "/subscribe";
    }





    /**
     * 职位收藏
     */


}
