package com.boss.bossframeworkweb.controller.admin;

import com.alibaba.fastjson.JSON;
import com.boss.bossframeworkweb.pojo.Page;
import com.boss.bossframeworkweb.pojo.development;
import com.boss.bossframeworkweb.pojo.field;
import com.boss.bossframeworkweb.service.impl.companyServiceimpl;
import com.boss.bossframeworkweb.service.impl.developmentServiceimpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class developmentController {
    @Resource
    developmentServiceimpl developmentServiceimpl;


    @Resource
    companyServiceimpl companylistService;

    @RequestMapping("/findAllDevelopment")
    @ResponseBody

    public String findAllDevelopment(){
        List<development>list= developmentServiceimpl.findAllDevelopment();
        String json = JSON.toJSONString(list);
        return json;
    }

}
