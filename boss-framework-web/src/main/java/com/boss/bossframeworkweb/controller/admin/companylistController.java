package com.boss.bossframeworkweb.controller.admin;


import com.alibaba.fastjson.JSON;
import com.boss.bossframeworkweb.pojo.Page;
import com.boss.bossframeworkweb.pojo.company;
import com.boss.bossframeworkweb.pojo.province;
import com.boss.bossframeworkweb.pojo.workplace;
import com.boss.bossframeworkweb.service.PositionGzService;
import com.boss.bossframeworkweb.service.impl.companyServiceimpl;
import com.boss.bossframeworkweb.service.impl.workplaceServiceimpl;
import com.boss.bossframeworkweb.service.provinceService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import javax.servlet.http.HttpServletRequest;


@Controller
public class companylistController {

    @Resource
    PositionGzService positionGzService;

    @Resource
    companyServiceimpl companylistService;


    @Resource
    private com.boss.bossframeworkweb.service.provinceService provinceService;

    @Resource
    com.boss.bossframeworkweb.service.impl.workplaceServiceimpl workplaceServiceimpl;


    @RequestMapping("/companyPageList")
    @ResponseBody
    public String companyPageList(HttpServletRequest request) {
        int totalCount = companylistService.totalCount(0, 0);
        Integer page = 0;
        if (request.getParameter("page") == null) {
            page = 1;
        } else {
            page = Integer.parseInt(request.getParameter("page"));

        }
        Page page1 = new Page();
        page1.setTotalCount(totalCount);
        page1.setCurrPageNo(page);
        page1.setPage((page - 1) * page1.getPageSize());
        List<company> list = companylistService.companyPageList(page1);
        String json = JSON.toJSONString(list);
        return json;
    }




    @RequestMapping("/totalCount")
    @ResponseBody
    public String totalCount(HttpServletRequest request, Integer dId, Integer fId) {
        int totalCount = 0;
        if (dId > 0) {
            totalCount = companylistService.totalCount(dId, 0);
        } else if (fId > 0) {
            totalCount = companylistService.totalCount(0, fId);
        }
        String json = JSON.toJSONString(totalCount);
        return json;
    }






    @RequestMapping("/companyTjPageList")
    @ResponseBody
    public String companyTjPageList(HttpServletRequest request, Integer dId, Integer fId) {
        String json = null;
        if (dId > 0) {
            int totalCount = companylistService.totalCount(dId, 0);
            Integer page = 0;
            if (request.getParameter("page") == null) {
                page = 1;
            } else {
                page = Integer.parseInt(request.getParameter("page"));
            }
            Page page1 = new Page();
            page1.setTotalCount(totalCount);
            page1.setCurrPageNo(page);
            page1.setPage((page - 1) * page1.getPageSize());
            page1.setdId(dId);
            List<company> list = companylistService.companyPageList(page1);
            request.getSession().removeAttribute("totalPageCount");
            request.setAttribute("totalPageCount", page1.getTotalPageCount());
            json = JSON.toJSONString(list);
        } else if (fId > 0) {
            int totalCount = companylistService.totalCount(0, fId);
            Integer page = 0;
            if (request.getParameter("page") == null) {
                page = 1;
            } else {
                page = Integer.parseInt(request.getParameter("page"));
            }
            Page page1 = new Page();
            page1.setTotalCount(totalCount);
            page1.setCurrPageNo(page);
            page1.setPage((page - 1) * page1.getPageSize());
            page1.setfId(fId);
            List<company> list = companylistService.companyPageList(page1);
            request.getSession().removeAttribute("totalPageCount");
            request.setAttribute("totalPageCount", page1.getTotalPageCount());
            json = JSON.toJSONString(list);
        }
        return json;
    }


    @RequestMapping("/workplacelist")
    public String workplace(HttpServletRequest request) {
        List<workplace> list = workplaceServiceimpl.workplacelist();
        request.setAttribute("workplacelist", list);
        List<province> list1 = provinceService.getAll();
        request.setAttribute("province", list1);

        int totalCount=companylistService.totalCount(0,0);
        Page page=new Page();
        page.setTotalCount(totalCount);
        request.setAttribute("totalPageCount",page.getTotalPageCount() );
        return "companylist";
    }


}

