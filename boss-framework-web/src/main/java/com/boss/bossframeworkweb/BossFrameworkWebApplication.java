package com.boss.bossframeworkweb;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;


@EnableEurekaClient
@SpringBootApplication
@MapperScan("com.boss.bossframeworkweb.mapper")
public class BossFrameworkWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(BossFrameworkWebApplication.class, args);
    }
}
