package com.boss.bossframeworkweb.pojo;/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.0
 */

/**
 * WORK_TITLE
 * 
 * @author bianj
 * @version 1.0.0 2020-06-16
 */
public class WorkTitle implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = -6868919202846012094L;

    /** 期望工资id */
    private int wordId;

    /** 地点 */
    private String site;

    /** 就职状态 */
    private String workState;

    /** 期望职位 */
    private String workName;

    /** 薪水 */
    private String workSalaried;

    /**
     * 获取期望工资id
     * 
     * @return 期望工资id
     */
    public Integer getWordId() {
        return this.wordId;
    }

    /**
     * 设置期望工资id
     * 
     * @param wordId
     *          期望工资id
     */
    public void setWordId(Integer wordId) {
        this.wordId = wordId;
    }

    /**
     * 获取地点
     * 
     * @return 地点
     */
    public String getSite() {
        return this.site;
    }

    /**
     * 设置地点
     * 
     * @param site
     *          地点
     */
    public void setSite(String site) {
        this.site = site;
    }

    /**
     * 获取就职状态
     * 
     * @return 就职状态
     */
    public String getWorkState() {
        return this.workState;
    }

    /**
     * 设置就职状态
     * 
     * @param workState
     *          就职状态
     */
    public void setWorkState(String workState) {
        this.workState = workState;
    }

    /**
     * 获取期望职位
     * 
     * @return 期望职位
     */
    public String getWorkName() {
        return this.workName;
    }

    /**
     * 设置期望职位
     * 
     * @param workName
     *          期望职位
     */
    public void setWorkName(String workName) {
        this.workName = workName;
    }

    /**
     * 获取薪水
     * 
     * @return 薪水
     */
    public String getWorkSalaried() {
        return this.workSalaried;
    }

    /**
     * 设置薪水
     * 
     * @param workSalaried
     *          薪水
     */
    public void setWorkSalaried(String workSalaried) {
        this.workSalaried = workSalaried;
    }
}