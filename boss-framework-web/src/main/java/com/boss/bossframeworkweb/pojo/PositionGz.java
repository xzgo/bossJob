package com.boss.bossframeworkweb.pojo;

import lombok.Data;

import java.sql.Date;
import java.util.List;

@Data
public class PositionGz {
    private  int id;
    private int category1;
    private  int category2;
    private  String name;

    private  String nature;
    private  int high;
    private  int low;
    private  String address;

    private  String workTime;
    private  String education;
    private  String attacts;
    private  String describe;

    private Date creationTime;
    private  int positionid;
    private  int hotposition;
    private  int companyid;

    private  String DateTime;
    private List<company>Companys;
    private  Date data;
}
