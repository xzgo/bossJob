package com.boss.bossframeworkweb.pojo;/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.0
 */

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * IMAGE_ADS
 * 
 * @author bianj
 * @version 1.0.0 2020-06-16
 */
@TableName("image_ads")
public class ImageAds implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = -2998486038332377763L;

    /** id */
    private Integer id;

    /** imagepath */
    private String imagepath;

    /** imagealt */
    private String imagealt;

    /**
     * 获取id
     * 
     * @return id
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * 设置id
     * 
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取imagepath
     * 
     * @return imagepath
     */
    public String getImagepath() {
        return this.imagepath;
    }

    /**
     * 设置imagepath
     * 
     * @param imagepath
     */
    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    /**
     * 获取imagealt
     * 
     * @return imagealt
     */
    public String getImagealt() {
        return this.imagealt;
    }

    /**
     * 设置imagealt
     * 
     * @param imagealt
     */
    public void setImagealt(String imagealt) {
        this.imagealt = imagealt;
    }
}