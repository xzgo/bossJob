package com.boss.bossframeworkweb.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.persistence.Id;
import java.util.List;

/**
 * 主页列表2
 */
@Data
@TableName("b")
public class job2 {
    @Id
    private  Integer bid;
    private  String  bname;
    private  Integer b_id;
    private List<job3> getc;
}
