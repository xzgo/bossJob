package com.boss.bossframeworkweb.pojo;/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.0
 */

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * 用户列表
 * 
 * @author bianj
 * @version 1.0.0 2020-06-03
 */
@Data
@Table(name = "user_login")
public class UserLogin implements java.io.Serializable {
    /** 版本号 */
    private static final long seriintalVersionUID = 8252003703501215237L;

    /** 序号 */
    private int  id;

    /** 名称 */
    private String name;

    /** 密码 */
    private String password;

    /** 手机号 */
    private String phone;

    /** 创建时间 */
    private Timestamp creationtime;

    /**短信验证码*/
    private  String   msg;

    /** 权限 */
    private String applyId;

}