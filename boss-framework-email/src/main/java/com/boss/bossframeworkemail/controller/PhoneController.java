package com.boss.bossframeworkemail.controller;



import com.boss.bossframeworkemail.pojo.UserLogin;
import com.boss.bossframeworkemail.service.PhoneService;
import com.boss.bossframeworkemail.utils.BodyType;
import com.boss.bossframeworkemail.utils.CCPRestSmsSDK;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.util.*;


@Api("短信注册")
@RestController
public class PhoneController {

	@Autowired
	private  PhoneService phoneService;


	/**
	 * 短信注册 加随机密码 用户名
	 * @param phone
	 * @return
	 */
	@RequestMapping(value = "/signin")
	@ResponseBody
	public String smsSdk(UserLogin phone) {
		System.out.println(phone);
		//生产环境请求地址：app.cloopen.com
		String serverIp = "app.cloopen.com";
		//请求端口
		String serverPort = "8883";
		//主账号,登陆云通讯网站后,可在控制台首页看到开发者主账号ACCOUNT SID和主账号令牌AUTH TOKEN
		String accountSId = "8a216da87249b81301726e9eb8af10ea";
		String accountToken = "9fb1ba8368b2496da83c8e02a2a86184";
		//请使用管理控制台中已创建应用的APPID
		String appId = "8a216da87249b81301726e9eb9a710f0";
		CCPRestSmsSDK sdk = new CCPRestSmsSDK();
		sdk.init(serverIp, serverPort);
		sdk.setAccount(accountSId, accountToken);
		sdk.setAppId(appId);
		sdk.setBodyType(BodyType.Type_JSON);
		String datas = String.valueOf(((new Random()).nextInt(899999)) + 100000);

		HashMap<String, Object> result = sdk.sendTemplateSMS(phone.getPhone(),"1",new String[]{datas,"5"});
		if("000000".equals(result.get("statusCode"))){
			//正常返回输出data包体信息（map）
			HashMap<String,Object> data = (HashMap<String, Object>) result.get("data");
			Set<String> keySet = data.keySet();
			for(String key:keySet){
				Object object = data.get(key);
				System.out.println(key +" = "+object);
			}
		}else{
			//异常返回输出错误码和错误信息
			System.out.println("错误码=" + result.get("statusCode") +" 错误信息= "+result.get("statusMsg"));
		}

		/*生成用户名*/
		String str = "";
		StringBuffer stringBuffer=new StringBuffer();
		for (int i = 0; i < 3; i++) {
			str =getRandomChar();
			stringBuffer.append(str);
		}
		phone.setName(stringBuffer.toString());
		/*生成密码*/
		StringBuffer Buffer=new StringBuffer();

		Random rand=new Random();//生成随机数
		for(int a=0;a<6;a++){
			String code="";
			code =rand.nextInt(10)+"";//生成6位验证码
			Buffer.append(code);
		}
		System.out.println(Buffer.toString());
		phone.setPassword(Buffer.toString());
		int i = phoneService.addPhone(phone);

		if (i>0){
			return "登录成功！";

		}
			return "登录失败！";

	}


	//随机生成用户名
	public static String getRandomChar() {
		String str = "";
		int highCode;
		int lowCode;
		Random random = new Random();

		highCode = (176 + Math.abs(random.nextInt(39))); //B0 + 0~39(16~55) 一级汉字所占区
		lowCode = (161 + Math.abs(random.nextInt(93))); //A1 + 0~93 每区有94个汉字

		byte[] b = new byte[2];
		b[0] = (Integer.valueOf(highCode)).byteValue();
		b[1] = (Integer.valueOf(lowCode)).byteValue();

		try {
			str = new String(b, "GBK");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return str;
	}
}
