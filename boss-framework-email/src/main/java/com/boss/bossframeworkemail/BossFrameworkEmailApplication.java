package com.boss.bossframeworkemail;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

@EnableEurekaClient
@SpringBootApplication
@MapperScan("com.boss.bossframeworkemail.mapper")
public class BossFrameworkEmailApplication {
    public static void main(String[] args) {
        SpringApplication.run(BossFrameworkEmailApplication.class, args);
    }

}
