package com.boss.bossframeworkcommon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BossFrameworkCommonApplication {
    public static void main(String[] args) {
        SpringApplication.run(BossFrameworkCommonApplication.class, args);
    }

}
